/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
 
window.Vue.use(VueRouter);
 
import App from './components/App'
import Home from './components/Home'
import Genre from './components/Genre'
import Artist from './components/Artist'
import Album from './components/Album'
import Song from './components/Song'
 
const router = new  VueRouter({
	mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/genres',
            name: 'genres',
            component: Genre
        },
        {
            path: '/artists',
            name: 'artists',
            component: Artist
        },
        {
            path: '/albums',
            name: 'albums',
            component: Album
        },
        {
            path: '/songs',
            name: 'songs',
            component: Song
        },
    ],
});
 
const app = new Vue({
	el: '#app',
   	components: { App },
    router,
});