@extends('layouts.app')
<style type="text/css">
    .add_new .label {
        text-align: right;
        line-height: 34px;
    }  
    .fa-star.empty {
        font-weight: 100;
    }
    div.table .row {
        line-height: 34px;
    }
    div.table .table-body .row:nth-child(odd) {
        background-color:  #f9f9f9;
    }
    span.favorite_button {
        cursor: pointer;
        padding-top: 11px;
    }
</style>
@section('content')
    <div class="table">
        @if(!empty($artists))
            <div class="table-header">
                <div class="row">
                    <div class="col-md-1 col-lg-1"></div>                
                    <div class="col-md-1 col-lg-1">
                        <label>ID</label>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <label>Name</label>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <label>Created</label>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <label>Updated</label>
                    </div>
                </div>  
            </div>
            <div class="table-body">
                @foreach($artists as $artist)
                    <div class="row">
                        <div class="col-md-1 col-lg-1">
                            @if(empty($artist['favorite']))
                                <span data-favorited="false" data-id="{{$artist->id}}"  data-type="artists" class="favorite_button fa fa-star empty" aria-hidden="true"></span>
                            @else
                                <span data-favorited="true" data-id="{{$artist->id}}"  data-type="artists" class="favorite_button fa fa-star" aria-hidden="true"></span>
                            @endif                        
                        </div>
                        <div class="col-md-1 col-lg-1">{{$artist->id}}</div>
                        <div class="col-md-3 col-lg-3">{{$artist->name}}</div>
                        <div class="col-md-3 col-lg-3">{{$artist->created_at}}</div>
                        <div class="col-md-3 col-lg-3">{{$artist->updated_at}}</div>
                        <div class="col-md-1 col-lg-1"></div>
                    </div>
                @endforeach   
            </div>                 
        @endif        
    </div>       
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('submit', '#add_new', function () { 
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data : $(this).serialize(),
                success: function(msg) {
                    console.log(msg.data);  
                },
                error : function(msg) {
                    alert(msg.responseText);
                }
            });            
            return false;
        });

        $(document).on('click', '.favorite_button', function () {
            if (!$(this).hasClass('loading')) {
                $(this).addClass('loading');
                var url = '/api/'+ $(this).data('type')+'/'+$(this).data('id')+'/favorites',
                    type = $(this).data('favorited'),
                    _this = this;                
                $.ajax({
                    type: type == true ? "DELETE" : "POST",
                    url: url,
                    success: function(data) {                            
                        if (type == true) {
                            $(_this).data('favorited', false).addClass('empty');
                        } else {
                            $(_this).data('favorited', true).removeClass('empty');
                        }
                    },
                    error : function(msg) {
                        alert(msg.responseText);
                    }
                });
                $(this).removeClass('loading');
            }
        });
    });    
</script>
@endsection 


