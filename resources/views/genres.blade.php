@extends('layouts.app')
<style type="text/css">
    .add_new .label {
        text-align: right;
        line-height: 34px;
    }  
    .fa-star.empty {
        font-weight: 100;
    }
    div.table .row {
        line-height: 34px;
    }
    div.table .table-body .row:nth-child(odd) {
        background-color:  #f9f9f9;
    }
    span.favorite_button {
        cursor: pointer;
        padding-top: 11px;
    }
</style>

@section('content')
    <router-view></router-view>
@endsection