@extends('layouts.app')

@section('content')
    {!! Form::open(['route' => 'store.Artist', 'id' => "add_new"]) !!}               
            <div class="col-md-2 col-lg-2 label">
                {{ Form::label("name", "Name :") }}
            </div> 
            <div class="col-md-3 col-lg-3"> 
                <div class="form-group">                                      
                    {{ Form::text("name", '', ['class'=>'form-control', 'placeholder' => "Enter Name"]) }}
                </div>
            </div>
            <div class="col-md-3 col-lg-3"> 
                {{ Form::submit("Add", ['class'=>'btn btn-primary']) }}
            </div>
    {!! Form::close() !!}        
@endsection