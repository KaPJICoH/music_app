@extends('layouts.app')

@section('content')
	@foreach($songs as $song)
        <div class="row">
            <div class="col-sm-1">
                @if(empty($song['favorite']))
                    <span data-favorited="false" data-id="{{$song->id}}"  data-type="songs" class="favorite_button fa fa-star-o" aria-hidden="true"></span>
                @else
                    <span data-favorited="true" data-id="{{$song->id}}"  data-type="songs" class="favorite_button fa fa-star" aria-hidden="true"></span>
                @endif                        
            </div>
            <div class="col-sm-1">{{$song->id}}</div>
            <div class="col-sm-3">{{$song->name}}</div>
            <div class="col-sm-3">{{$song->album->name}}</div>
            <div class="col-sm-3">
            	@if(empty($song->genre))
                   $song->genre->name
                @endif
            </div>
            <div class="col-sm-1"></div>
        </div>
    @endforeach
@endsection