<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Music app</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<style type="text/css">
		button {
			height: 34px;
		}
		
	</style>		
</head>
<body>
	@yield('content')
	<script src="{{ asset('js/app.js') }}"></script>
	@yield('script')
</body>
</html>