@extends('layouts.app')

@section('content')
    @foreach($albums as $album)
        <div class="row">
            <div class="col-sm-1">
                @if(empty($album['favorite']))
                    <span data-favorited="false" data-id="{{$album->id}}"  data-type="albums" class="favorite_button fa fa-star-o" aria-hidden="true"></span>
                @else
                    <span data-favorited="true" data-id="{{$album->id}}"  data-type="albums" class="favorite_button fa fa-star" aria-hidden="true"></span>
                @endif                        
            </div>
            <div class="col-sm-1">{{$album->id}}</div>
            <div class="col-sm-3">{{$album->name}}</div>
            <div class="col-sm-2">{{$album->artist->name}}</div>
            <div class="col-sm-2">{{$album->created_at}}</div>
            <div class="col-sm-2">{{$album->updated_at}}</div>
            <div class="col-sm-1"></div>
        </div>
    @endforeach
@endsection