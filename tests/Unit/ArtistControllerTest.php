<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Artist;

class ArtistControllerTest extends TestCase
{
	use DatabaseTransactions;

	public function testIndex()
	{
		$response = $this->json('get', '/api/artists');
		$response->assertStatus(200)
			->assertJsonCount(Artist::all()->count(), 'data');
	}

	public function testShow()
	{
		$artist = factory(App\Artist::class, 1)->create();
		$response = $this->json('get', '/api/artists/'.$artist->first()->id);
		$response->assertStatus(200)
			->assertJson([
				'data' => $artist->first()->toArray()
			]);
	}

	public function testStore()
	{
		
		$response = $this->json('post', '/api/artists', [
			'name' => 'test_artist'
		]);
		$response->assertStatus(201)
			->assertJson([
				'data' => [ 
					'name' => 'test_artist'
				]
			]);
	}

	public function testUpdate()
	{
		$artist = factory(App\Artist::class, 1)->create();
		$response = $this->json('put', '/api/artists/'.$artist->first()->id, ['name' => 'changed_name']);
		$response->assertStatus(200)
			->assertJson([
				'data' => [ 
					'name' => 'changed_name'
				]
			]);
	}

	public function testUpdateWithWrongID()
	{
		$response = $this->json('put', '/api/artists/0', ['name' => 'changed_name']);
		$response->assertStatus(404);
	}

	public function testDelete()
	{
		$artist = factory(App\Artist::class, 1)->create();
		$response = $this->json('delete', '/api/artists/'.$artist->first()->id);
		$response->assertStatus(204);
	}

	public function testDeleteWithWrongID()
	{
		$response = $this->json('delete', '/api/artists/0');
		$response->assertStatus(404);
	}
}