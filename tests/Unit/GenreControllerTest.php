<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Genre;

class GenreControllerTest extends TestCase
{
	use DatabaseTransactions;

	public function testIndex()
	{
		$response = $this->json('get', '/api/genres');
		$response->assertStatus(200)
			->assertJsonCount(Genre::all()->count(), 'data');
	}

	public function testShow()
	{
		$genre = factory(App\Genre::class, 1)->create();
		$response = $this->json('get', '/api/genres/'.$genre->first()->id);
		$response->assertStatus(200)
			->assertJson([
				'data' => $genre->first()->toArray()
			]);
	}

	public function testStore()
	{
		$response = $this->json('post', '/api/genres', [
			'name' => 'test_genre'
		]);
		$response->assertStatus(201)
			->assertJson([
				'data' => [ 
					'name' => 'test_genre'
				]
			]);
	}

	public function testUpdate()
	{
		$genre = factory(App\Genre::class, 1)->create();
		$response = $this->json('put', '/api/genres/'.$genre->first()->id, ['name' => 'changed_genre']);
		$response->assertStatus(200)
			->assertJson([
				'data' => [ 
					'name' => 'changed_genre'
				]
			]);
	}

	public function testUpdateWithWrongID()
	{
		$response = $this->json('put', '/api/genres/0', ['name' => 'changed_genre']);
		$response->assertStatus(404);
	}

	public function testDelete()
	{
		$genre = factory(App\Genre::class, 1)->create();
		$response = $this->json('delete', '/api/genres/'.$genre->first()->id);
		$response->assertStatus(204);
	}

	public function testDeleteWithWrongID()
	{
		$response = $this->json('delete', '/api/genres/0');
		$response->assertStatus(404);
	}
}