<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Song;

class SongControllerTest extends TestCase
{
	use DatabaseTransactions;

	public function testIndex()
	{
		$response = $this->json('get', '/api/songs');
		$response->assertStatus(200)
			->assertJsonCount(Song::all()->count(), 'data');
	}

	public function testShow()
	{
		
		$song = factory(App\Song::class, 1)->create();
		$response = $this->json('get', '/api/songs/'.$song->first()->id);
		$response->assertStatus(200)
			->assertJson([
				'data' => $song->first()->toArray()
			]);
	}

	public function testStore()
	{
		$album = factory(App\Album::class, 1)->create();
		$response = $this->json('post', '/api/albums/'.$album->first()->id.'/songs', [
			'name' => 'test_song'
		]);
		$response->assertStatus(201)
			->assertJson([
				'data' => [ 
					'name' => 'test_song'
				]
			]);
	}

	public function testUpdate()
	{
		$song = factory(App\Song::class, 1)->create();
		$response = $this->json('put', '/api/songs/'.$song->first()->id, ['name' => 'changed_song']);
		$response->assertStatus(200)
			->assertJson([
				'data' => [ 
					'name' => 'changed_song'
				]
			]);
	}

	public function testUpdateWithWrongID()
	{
		$response = $this->json('put', '/api/songs/0', ['name' => 'changed_song']);
		$response->assertStatus(404);
	}

	public function testDelete()
	{
		$song = factory(App\Song::class, 1)->create();
		$response = $this->json('delete', '/api/songs/'.$song->first()->id);
		$response->assertStatus(204);
	}

	public function testDeleteWithWrongID()
	{
		$response = $this->json('delete', '/api/songs/0');
		$response->assertStatus(404);
	}
}