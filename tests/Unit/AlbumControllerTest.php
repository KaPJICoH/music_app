<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Album;
class AlbumControllerTest extends TestCase
{
	use DatabaseTransactions;

	public function testIndex()
	{
		$response = $this->json('get', '/api/albums');
		$response->assertStatus(200)
			->assertJsonCount(Album::all()->count(), 'data');
	}

	public function testShow()
	{
		$album = factory(App\Album::class)->create();
		$response = $this->json('get', '/api/albums/'.$album->first()->id);
		$response->assertStatus(200)
			->assertJson([
				'data' => $album->first()->toArray()
			]);
	}

	public function testStore()
	{
		$artist = factory(App\Artist::class, 1)->create();
		$response = $this->json('post', '/api/artists/'.$artist->first()->id.'/albums', [
			'name' => 'test_album'
		]);
		$response->assertStatus(201)
			->assertJson([
				'data' => [ 
					'name' => 'test_album',
					'artist_id' => $artist->first()->id
				]
			]);
	}

	public function testUpdate()
	{
		$album = factory(App\Album::class)->create();
		$response = $this->json('put', '/api/albums/'.$album->first()->id, ['name' => 'changed_name']);
		$response->assertStatus(200)
			->assertJson([
				'data' => [ 
					'name' => 'changed_name'
				]
			]);
	}

	public function testUpdateWithWrongId()
	{			
		$response = $this->json('put', '/api/albums/0', ['name' => 'changed_name']);
		$response->assertStatus(404);
	}

	public function testDelete()
	{
		$album = factory(App\Album::class)->create();
		$response = $this->json('delete', '/api/albums/'.$album->first()->id);
		$response->assertStatus(204);
	}

	public function testDeleteWithWrongId()
	{			
		$response = $this->json('delete', '/api/albums/0');
		$response->assertStatus(404);
	}
}