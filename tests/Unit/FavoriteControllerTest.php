<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Favorite;
use App\Song;

class FavoriteControllerTest extends TestCase
{
	use DatabaseTransactions;

	public function testIndex()
	{
		$response = $this->json('get', '/api/favorites');
		$response->assertStatus(200)
			->assertJsonCount(Favorite::all()->count(), 'data');
	}	

	public function testgetByType()
	{
		$album = factory(App\Album::class, 1)->create();
		$response = $this->json('get', '/api/songs/favorites');
		$response->assertStatus(200)
			->assertJsonCount(Song::has('favorite')->get()->count(), 'data');;
	}

	public function testStore()
	{
		$album = factory(App\Album::class, 1)->create();
		$response = $this->json('post', '/api/albums/'.$album->first()->id.'/favorites');
		$response->assertStatus(201);
	}

	public function testDelete()
	{
		$song = factory(App\Song::class, 1)->create();
		$response = $this->json('delete', '/api/songs/'.$song->first()->id.'/favorites');
		$response->assertStatus(204);
	}
}