<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Artist::class, function (Faker $faker) {
    return [
        'name' => 'Artist-'.$faker->name,        
    ];
});

$factory->define(App\Album::class, function (Faker $faker) {
    return [
        'name' => 'Album-'.$faker->name,
        'artist_id' => factory(App\Artist::class)->create()->id,            
    ];
});

$factory->define(App\Song::class, function (Faker $faker) {
    return [
        'name' => 'Song-'.$faker->address,
        'album_id' => factory(App\Album::class)->create()->id,
        'genre_id' => factory(App\Genre::class)->create()->id,          
    ];
});

$factory->define(App\Genre::class, function (Faker $faker) {
    return [
        'name' => 'Genre-'.$faker->name,     
    ];
});

$factory->afterCreating(App\Artist::class, function ($artist, $faker) {
    if (rand(1,10) > 7) {
        $artist->favorite()->create([]);
    }
});

$factory->afterCreating(App\Album::class, function ($album, $faker) {
    if (rand(1,10) > 7) {
        $album->favorite()->create([]);
    }
});

$factory->afterCreating(App\Song::class, function ($song, $faker) {
    if (rand(1,10) > 7) {
        $song->favorite()->create([]);
    }
});

