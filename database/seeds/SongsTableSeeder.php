<?php

use Illuminate\Database\Seeder;
use App\Album;

class SongsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(App\Song::class, 100)->create();
    }
}
