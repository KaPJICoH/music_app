<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\ElasticSearch\Searchable;
use App\Helpers\ElasticSearch\SearchableModel;

class Song extends Model implements SearchableModel
{
    use Searchable;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'album_id', 'genre_id'];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function album()
    {
        return $this->belongsTo(Album::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphOne
     */
    public function favorite()
    {
        return $this->morphOne(Favorite::class, 'favoriteable');
    }
    
    /**
     * @param string $query
     * @return array
     */
    public function getElasctiQuery(string $query) : array 
    {
        return [
            'match' => [
                "name" => $query                    
            ]
        ];
    }
}
