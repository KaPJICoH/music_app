<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\ElasticSearch\Searchable;
use App\Helpers\ElasticSearch\SearchableModel;

class Genre extends Model implements SearchableModel
{
    use Searchable;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @param string $query
     * @return array
     */
    public function getElasctiQuery(string $query) : array 
    {
        return [
            'match' => [
                "name" => $query                    
            ]
        ];
    }
}
