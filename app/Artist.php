<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\ElasticSearch\Searchable;
use App\Helpers\ElasticSearch\SearchableModel;

class Artist extends Model implements SearchableModel
{
    use Searchable;

   	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function albums()
    {
        return $this->hasMany(Album::class);
    }
    
    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphOne
     */
    public function favorite()
    {
        return $this->morphOne(Favorite::class, 'favoriteable');
    }

    /**
     * @param string $query
     * @return array
     */
    public function getElasctiQuery(string $query) : array 
    {
        return [
            'match' => [
                "name" => $query                    
            ]
        ];
    }
}
