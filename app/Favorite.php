<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
	public $timestamps = false;
    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphTo
     */
    public function favoriteable()
    {
        return $this->morphTo();
    }
}
