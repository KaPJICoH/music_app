<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\ElasticSearch\ElasticClient;
use Elasticsearch\ClientBuilder;
use App\{Artist, Album, Song, Genre};
use App\Observers\{ArtistObserver, AlbumObserver, SongObserver, GenreObserver};

class ElasticServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Artist::observe(ArtistObserver::class);
        Album::observe(AlbumObserver::class);
        Song::observe(SongObserver::class);
        Genre::observe(GenreObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ElasticClient::class, function ($app) {
            return new ElasticClient( ClientBuilder::create()->setHosts(config('services.elasticsearch.hosts'))->build() );
        });
    }
}
