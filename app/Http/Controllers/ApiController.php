<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Throwable;

class ApiController extends Controller
{
	/**
     * @var
     */
    protected $statusCode = 200;
    protected $errors = [];
    protected $response = [];

    /**
     * @param $error
     */
    protected function addError(Throwable $error) : void
    {
        $this->addCustomError($error->getMessage(), $error->getCode());
    }

    /**
     * @param $message
     * @param int $code
     */
    protected function addCustomError(string $message, int $code = 400) : void
    {
        $this->statusCode = $code;
        array_push($this->errors, [
            'status_code' => $code,
            'status_code' => $code,
            'message' => $message
        ]);
    }

    /**
     * @return json
     */
    protected function getResponse()
    {
        return response()->json([
            'data' => $this->response,
            'errors' => $this->errors,
            'status_code' => $this->statusCode
        ], $this->statusCode);
    }
}
