<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\ArtistCollection;
use App\Helpers\ElasticSearch\EloquentRepository;
use App\Artist;

class ArtistController extends ApiController
{
	/**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {         	      		
        $this->response = Artist::all();        
        return $this->getResponse();
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {                  
        $this->response = Artist::find($id);
        return $this->getResponse(); 
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {  
                         
        $this->response = Artist::create($request->all());
        $this->statusCode = 201;
        return $this->getResponse();      
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id)
    {                 
        $article = Artist::findOrFail($id);
        $article->update($request->all());
        $this->response = $article;
        return $this->getResponse();      
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {                           
        $article = Artist::findOrFail($id);
        $article->delete();
        return response()->json(null, 204);                                   
    }

    /**
     * @param string $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(string $name)
    {                           
        $this->response = (new EloquentRepository(new Artist))->search((string)$name);
        return $this->getResponse();                                 
    }
}
