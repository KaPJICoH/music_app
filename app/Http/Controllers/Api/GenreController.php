<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Helpers\ElasticSearch\EloquentRepository;
use App\Genre;

class GenreController extends ApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)//@todo add pagination
    {    		
        $this->response = Genre::all();
        return $this->getResponse();
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {                    
        $this->response = Genre::find($id); 
        return $this->getResponse();                   
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {                  
        $this->response = Genre::create($request->all());
        $this->statusCode = 201; 
        return $this->getResponse();                                
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id)
    {                  
        $article = Genre::findOrFail($id);
        $article->update($request->all());
        $this->response = $article;
        return $this->getResponse();                               
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {                        
        $article = Genre::findOrFail($id);
        $article->delete();
        return response()->json(null, 204);                                  
    }

    /**
     * @param string $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(string $name)
    {                           
        $this->response = (new EloquentRepository(new Genre))->search((string)$name);
        return $this->getResponse();                                 
    }
}
