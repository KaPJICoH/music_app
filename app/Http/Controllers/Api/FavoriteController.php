<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\{Favorite, Song, Album, Artist};
use App\Http\Resources\FavoriteCollection as FavoriteResources;

class FavoriteController extends ApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {    		
        $this->response =  new FavoriteResources(Favorite::all());
        return $this->getResponse(); 
    }

    /**
     * @param Request $request
     * @param string $favorite_type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFavoritesByFavoriteType(Request $request, string $favorite_type)
    {     		
        $this->response = $this->getModelsByFavoritesType($favorite_type)->has('favorite')->get(); 
        return $this->getResponse();                               
    }

    /**
     * @param Request $request
     * @param string $favorite_type
     * @param string $favorite_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, string $favorite_type, int $favorite_id)
    {      		
        $this->getModelsByFavoritesType($favorite_type)->findOrFail($favorite_id)->favorite()->create([]);
        return response()->json(null, 201); 
    }

    /**
     * @param Request $request
     * @param string $favorite_type
     * @param string $favorite_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, string $favorite_type, int $favorite_id)
    {     		
        $this->getModelsByFavoritesType($favorite_type)->findOrFail($favorite_id)->favorite()->delete();  
        return response()->json(null, 204); 
    }
    
    /**     
     * @param string $favorite_type     
     * @return Song|Artist|Album
     */
    private function getModelsByFavoritesType($favorite_type) {//@todo change
    	switch ($favorite_type) {
    		case 'songs':
    			return new Song;
    		case 'artists':
    			return new Artist;    			
    		case 'albums':
    			return new Album;  		
    	}
    	throw new \Exception("Something went wrong", 400); 
    }
}
