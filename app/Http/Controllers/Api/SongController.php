<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Helpers\ElasticSearch\EloquentRepository;
use App\Song;
use App\Album;

class SongController extends ApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()//@todo add pagination
    {     		
        $this->response = Song::all(); 
        return $this->getResponse();                            
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    { 	                    
        $this->response = Song::find($id); 
        return $this->getResponse();                                
    }

    /**
     * @param Request $request
     * @param int $albom_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, int $albom_id)
    {                 
        $this->response = Album::findOrFail($albom_id)->songs()->create($request->all());
        $this->statusCode = 201; 
        return $this->getResponse();                               
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id)
    {                  
        $article = Song::findOrFail($id);
        $article->update($request->all());
        $this->response = $article;
        return $this->getResponse();                              
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {                     
        $article = Song::findOrFail($id);
        $article->delete();
        return response()->json(null, 204);                    
    }

    /**
     * @param string $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(string $name)
    {                           
        $this->response = (new EloquentRepository(new Song))->search((string)$name);
        return $this->getResponse();                                 
    }
}
