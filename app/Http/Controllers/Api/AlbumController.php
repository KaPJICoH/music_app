<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Helpers\ElasticSearch\EloquentRepository;
use App\Album;
use App\Artist;

class AlbumController extends ApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()//@todo add pagination
    {        		
        $this->response = Album::all(); 
        return $this->getResponse();                              
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {         	                    
        $this->response = Album::find($id); 
        return $this->getResponse();                                
    }

    /**
     * @param Request $request
     * @param int $artist_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, int $artist_id)
    {                   
        $this->response = Artist::findOrFail($artist_id)->albums()->create($request->all());
        $this->statusCode = 201; 
        return $this->getResponse();                                       
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id)
    {                           
        $article = Album::findOrFail($id);
        $article->update($request->all());
        $this->response = $article;
        return $this->getResponse(); 
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {                      
        $article = Album::findOrFail($id);
        $article->delete();
        return response()->json(null, 204);                                      
    }

    /**
     * @param string $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(string $name)
    {                           
        $this->response = (new EloquentRepository(new Album))->search((string)$name);
        return $this->getResponse();                                 
    }
}
