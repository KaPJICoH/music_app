<?php

namespace App\Observers;

use App\Song;
use App\Helpers\ElasticSearch\ElasticClient;

class SongObserver
{
    private $elastic;

    public function __construct ()
    {
        $this->elastic = resolve(ElasticClient::class);
    }

    /**
     * Handle the song "created" event.
     *
     * @param  \App\Song  $song
     * @return void
     */
    public function created(Song $song)
    {
        $this->elastic->index([
            'index' => $song->getSearchIndex(),
            'type' => $song->getSearchType(),
            'id' => $song->id,
            'body' => [
                'name' => $song->name,
                'album_name' =>  $song->album->name,
                'artist_name' =>  $song->album->artist->name,
                'genre_name' =>  $song->genre->name ?? ''
            ]
        ]);
        
    }

    /**
     * Handle the song "updated" event.
     *
     * @param  \App\Song  $song
     * @return void
     */
    public function updated(Song $song)
    {
        $this->elastic->index([
            'index' => $song->getSearchIndex(),
            'type' => $song->getSearchType(),
            'id' => $song->id,
            'body' => [
                'name' => $song->name,
                'album_name' =>  $song->album->name,
                'artist_name' =>  $song->album->artist->name,
                'genre_name' =>  $song->genre->name ?? ''
            ]
        ]);
    }

    /**
     * Handle the song "deleted" event.
     *
     * @param  \App\Song  $song
     * @return void
     */
    public function deleted(Song $song)
    {
        $this->elastic->delete([
            'index' => $song->getSearchIndex(),
            'type' => $song->getSearchType(),
            'id' => $song->id
        ]);
    }
}
