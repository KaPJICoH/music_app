<?php

namespace App\Observers;

use App\Album;
use App\Helpers\ElasticSearch\ElasticClient;

class AlbumObserver
{
    private $elastic;

    public function __construct ()
    {
        $this->elastic = resolve(ElasticClient::class);
    }
    /**
     * Handle the album "created" event.
     *
     * @param  \App\Album  $album
     * @return void
     */
    public function created(Album $album)
    {
        $this->elastic->index([
            'index' => $album->getSearchIndex(),
            'type' => $album->getSearchType(),
            'id' => $album->id,
            'body' => [
                'name' => $album->name,
                'artist_name' =>  $album->artist->name
            ]
        ]);
    }

    /**
     * Handle the album "updated" event.
     *
     * @param  \App\Album  $album
     * @return void
     */
    public function updated(Album $album)
    {
        $this->elastic->index([
            'index' => $album->getSearchIndex(),
            'type' => $album->getSearchType(),
            'id' => $album->id,
            'body' => [
                'name' => $album->name,
                'artist_name' =>  $album->artist->name
            ]
        ]);
    }

    /**
     * Handle the album "deleted" event.
     *
     * @param  \App\Album  $album
     * @return void
     */
    public function deleted(Album $album)
    {
        $this->elastic->delete([
            'index' => $album->getSearchIndex(),
            'type' => $album->getSearchType(),
            'id' => $album->id
        ]);
    }
}
