<?php

namespace App\Observers;

use App\Genre;
use App\Helpers\ElasticSearch\ElasticClient;

class GenreObserver
{
    private $elastic;

    public function __construct ()
    {
        $this->elastic = resolve(ElasticClient::class);
    }

    /**
     * Handle the genre "created" event.
     *
     * @param  \App\Genre  $genre
     * @return void
     */
    public function created(Genre $genre)
    {
        $this->elastic->index([
            'index' => $genre->getSearchIndex(),
            'type' => $genre->getSearchType(),
            'id' => $genre->id,
            'body' => [
                'name' => $genre->name
            ]
        ]);
    }

    /**
     * Handle the genre "updated" event.
     *
     * @param  \App\Genre  $genre
     * @return void
     */
    public function updated(Genre $genre)
    {
        $this->elastic->index([
            'index' => $genre->getSearchIndex(),
            'type' => $genre->getSearchType(),
            'id' => $genre->id,
            'body' => [
                'name' => $genre->name
            ]
        ]);
    }

    /**
     * Handle the genre "deleted" event.
     *
     * @param  \App\Genre  $genre
     * @return void
     */
    public function deleted(Genre $genre)
    {
        $this->elastic->delete([
            'index' => $genre->getSearchIndex(),
            'type' => $genre->getSearchType(),
            'id' => $genre->id
        ]);
    }
}
