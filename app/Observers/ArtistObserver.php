<?php

namespace App\Observers;

use App\Artist;
use App\Helpers\ElasticSearch\ElasticClient;

class ArtistObserver
{
    private $elastic;

    public function __construct ()
    {
        $this->elastic = resolve(ElasticClient::class);
    }
    
    /**
     * Handle the artist "created" event.
     *
     * @param  \App\Artist  $artist
     * @return void
     */
    public function created(Artist $artist)
    {
        $this->elastic->index([
            'index' => $artist->getSearchIndex(),
            'type' => $artist->getSearchType(),
            'id' => $artist->id,
            'body' => [
                'name' => $artist->name
            ]
        ]);
    }

    /**
     * Handle the artist "updated" event.
     *
     * @param  \App\Artist  $artist
     * @return void
     */
    public function updated(Artist $artist)
    {
        $this->elastic->index([
            'index' => $artist->getSearchIndex(),
            'type' => $artist->getSearchType(),
            'id' => $artist->id,
            'body' => $artist->toArray()
        ]);
    }

    /**
     * Handle the artist "deleted" event.
     *
     * @param  \App\Artist  $artist
     * @return void
     */
    public function deleted(Artist $artist)
    {
        $this->elastic->delete([
            'index' => $artist->getSearchIndex(),
            'type' => $artist->getSearchType(),
            'id' => $artist->id
        ]);
    }
}
