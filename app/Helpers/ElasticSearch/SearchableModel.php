<?php

namespace App\Helpers\ElasticSearch;

use Illuminate\Database\Eloquent\Collection;

interface SearchableModel
{
    public function getElasctiQuery(string $query) : array;
}