<?php

namespace App\Helpers\ElasticSearch;

trait Searchable
{
    /**
     * @return string
     */
    public function getSearchIndex() : string
    {
        return $this->getTable();
    }

    /**
     * @return string
     */
    public function getSearchType() : string
    {
        if (property_exists($this, 'useSearchType')) {
            return $this->useSearchType;
        }

        return $this->getTable();
    }

    /**
     * @return array
     */
    public function toSearchArray() : array
    {
        return $this->toArray();
    }
}