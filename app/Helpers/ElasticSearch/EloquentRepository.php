<?php

namespace App\Helpers\ElasticSearch;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\ElasticSearch\ElasticClient;

class EloquentRepository
{
    private $search;
    private $model;

    public function __construct(SearchableModel $model) {
        $this->model = $model;
        $this->search = resolve(ElasticClient::class);
    }

    public function search(string $query = ""): Collection
    {
        $items = $this->searchOnElasticsearch($query);

        return $this->buildCollection($items);          
    }

    private function searchOnElasticsearch(string $query): array
    {
        $items = $this->search->search([
            'index' => $this->model->getSearchIndex(),
            'type' => $this->model->getSearchType(),
            'body' => [
                //@todo not sure about it
                //"size" => 100,
                'query' => $this->model->getElasctiQuery($query),                
            ],
        ]);

        return $items;
    }

    private function buildCollection(array $items): Collection
    {        
        $id = array_pluck($items['hits']['hits'], '_id') ?: [];
        return $this->model->whereIn('id', $id)->get();        
    }
    
    //@todo not sure about it
    // Secound way to use Elasticsearch
    // private function buildCollection(array $items): Collection
    // {
    //     $hits = array_pluck($items['hits']['hits'], '_source') ?: [];

    //     $sources = array_map(function ($source) {            
    //         $source['tags'] = json_encode($source['tags']);
    //         return $source;
    //     }, $hits);

    //     return $this->model->hydrate($sources);
    // }
}