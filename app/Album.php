<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\ElasticSearch\Searchable;
use App\Helpers\ElasticSearch\SearchableModel;

class Album extends Model implements SearchableModel
{

    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'artist_id'];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function artist()
    {
        return $this->belongsTo(Artist::class);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function songs()
    {
        return $this->hasMany(Song::class);
    }
    
    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphOne
     */
    public function favorite()
    {
        return $this->morphOne(Favorite::class, 'favoriteable');
    }
    
    /**
     * @param string $query
     * @return array
     */
    public function getElasctiQuery(string $query) : array 
    {
        return [
            'match' => [
                "name" => $query                    
            ]
        ];
    }
}
