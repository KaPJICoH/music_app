<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| Favorite
|--------------------------------------------------------------------------
*/
	Route::get('/favorites', [
		'as'=>'index.Favorite',
	    'uses'=>'Api\FavoriteController@index'
	]);	
	Route::get('/{favorite_type}/favorites', [
		'as'=>'get.favorites',
	    'uses'=>'Api\FavoriteController@getFavoritesByFavoriteType'
	]);
	Route::post('/{favorite_type}/{id}/favorites', [
		'as'=>'store.Favorite',
	    'uses'=>'Api\FavoriteController@store'
	]);	
	Route::delete('/{favorite_type}/{id}/favorites', [
	    'as'=>'delete.Favorite',
	    'uses'=>'Api\FavoriteController@delete'
	]);
/*
|--------------------------------------------------------------------------
| Artist
|--------------------------------------------------------------------------
*/
	Route::get('/artists', [
		'as'=>'index.Artist',
	    'uses'=>'Api\ArtistController@index'
	]);	

	Route::get('/artists/{artist_id}', [
		'as'=>'show.Artist',
	    'uses'=>'Api\ArtistController@show'
	])->where(['artist_id' => '[0-9]+']);

	Route::get('/artists/{artist_name}', [
		'as'=>'search.Artist',
	    'uses'=>'Api\ArtistController@search'
	]);

	Route::post('/artists', [
	    'as'=>'store.Artist',
	    'uses'=>'Api\ArtistController@store'
	]);

	Route::put('/artists/{artist}', [
	    'as'=>'update.Artist',
	    'uses'=>'Api\ArtistController@update'
	]);

	Route::delete('/artists/{artist}', [
		'as'=>'delete.Artist',
	    'uses'=>'Api\ArtistController@delete'
	]);
/*
|--------------------------------------------------------------------------
| Genre
|--------------------------------------------------------------------------
*/
	Route::get('/genres', [
		'as'=>'index.Genre',
	    'uses'=>'Api\GenreController@index'
	]);

	Route::get('/genres/{genre_id}', [
		'as'=>'show.Genre',
	    'uses'=>'Api\GenreController@show'
	])->where(['genre_id' => '[0-9]+']);

	Route::get('/genres/{genre_name}', [
		'as'=>'search.Genre',
	    'uses'=>'Api\GenreController@search'
	]);

	Route::post('/genres', [
	    'as'=>'store.Genre',
	    'uses'=>'Api\GenreController@store'
	]);

	Route::put('/genres/{genre}', [
	    'as'=>'update.Genre',
	    'uses'=>'Api\GenreController@update'
	]);

	Route::delete('/genres/{genre}', [
		'as'=>'delete.Genre',
	    'uses'=>'Api\GenreController@delete'
	]);

/*
|--------------------------------------------------------------------------
| Album
|--------------------------------------------------------------------------
*/
//@todo add get albums by artist
// Route::get('/artists/{artist}/album'
	Route::get('/albums', [
		'as'=>'index.Album',
	    'uses'=>'Api\AlbumController@index'
	]);

	Route::get('/albums/{album_id}', [
		'as'=>'show.Album',
	    'uses'=>'Api\AlbumController@show'
	])->where(['album_id' => '[0-9]+']);

	Route::get('/albums/{album_name}', [
		'as'=>'search.Album',
	    'uses'=>'Api\AlbumController@search'
	]);

	Route::delete('/albums/{album}', [
		'as'=>'delete.Album',
	    'uses'=>'Api\AlbumController@delete'
	]);

	Route::post('/artists/{artist}/albums', [
	    'as'=>'store.Album',
	    'uses'=>'Api\AlbumController@store'
	]);

	Route::put('/albums/{artist}', [
	    'as'=>'update.Album',
	    'uses'=>'Api\AlbumController@update'
	]);
/*
|--------------------------------------------------------------------------
| Song
|--------------------------------------------------------------------------
*/
//@todo add get songs by albums
// Route::get('/albums/{album}/songs'
	Route::get('/songs', [
		'as'=>'index.Song',
	    'uses'=>'Api\SongController@index'
	]);

	Route::get('/songs/{song_id}', [
		'as'=>'show.Song',
	    'uses'=>'Api\SongController@show'
	])->where(['song_id' => '[0-9]+']);

	Route::get('/songs/{song_name}', [
		'as'=>'search.Song',
	    'uses'=>'Api\SongController@search'
	]);

	Route::delete('/songs/{song}', [
		'as'=>'delete.Song',
	    'uses'=>'Api\SongController@delete'
	]);

	Route::post('/albums/{albom}/songs', [
	    'as'=>'store.Song',
	    'uses'=>'Api\SongController@store'
	]);

	Route::put('/songs/{song}', [
	    'as'=>'update.Song',
	    'uses'=>'Api\SongController@update'
	]);

