# How start #

For start working with this project you need those steps:

- Build dockers
- install dependencies via Composer
- Create .env file. You can found example this file in main directory.
- Run command for key generating "docker-compose exec app php artisan key:generate".
- Run migreate with seeds "docker-compose exec app php artisan migrate --seed".

# About API #

## Artist ##

### For get all artists: ###
- url get: "/api/artists"

### For get artist by id: ###
- url get: "/api/artists/{id}"

### For get artist by artist's name: ###
- url get: "/api/artists/{artist_name}"

### For add artist: ###
- url post: "/api/artists"
- param $name.

### For chang artist: ###
- url put: "/api/artists/{id}"
- param $name.

### For delete artist: ###
- url delete: "/api/artists/{id}"

## Album ##

### For get all albums: ###
- url get: "/api/albums"

### For get album by id: ###
- url get: "/api/albums/{id}"

### For get album by album's name: ###
- url get: "/api/albums/{album_name}"

### For add album: ###
- url post: "/artists/{artist_id}/albums"
- param $name.

### For chang album: ###
- url put: "/api/albums/{id}"
- param $name.

### For delete album: ###
- url delete: "/api/albums/{id}"

## Song ##

### For get all songs: ###
- url get: "/api/songs"

### For get song by id: ###
- url get: "/api/songs/{id}"

### For get song by song's name: ###
- url get: "/api/songs/{song_name}"

### For add song: ###
- url post: "/albums/{album_id}/songs"
- param $name.
- param $genre_id.

### For chang song: ###
- url put: "/api/songs/{id}"
- param $name.
- param $genre_id.

### For delete song: ###
- url delete: "/api/songs/{id}"

## Genre ##

### For get all genres: ###
- url get: "/api/genres"

### For get genre by id: ###
- url get: "/api/genres/{id}"

### For get genre by genre's name: ###
- url get: "/api/genres/{genre_name}"

### For add genre: ###
- url post: "/api/genres"
- param $genre_id.

### For change genre: ###
- url put: "/api/genres/{id}"
- param $name.

### For delete genre: ###
- url delete: "/api/genres/{id}"


## Favorite ##

### For get all favorites: ###
- url get: "/api/favorites"

### For get favorites by favorite_type: ###
- url get: "/api/{favorite_type}/favorites"

### For add to favorites : ###
- url post: "/api/{favorite_type}/{id}/favorites"

### For delete to favorites : ###
- url delete: "/api/{favorite_type}/{id}/favorites"

